import { School } from '../models/School';
import { UUID } from 'angular2-uuid';

/**
 * Prospect class which is a subtype of School
 * @author Julio Bravo <jbravo@clb.unoi.com>
 */

export class Prospect extends School{

  private address : string;
  private pobox: number;
  private locality: string;
  private town: string;
  private state: string;
  private country: string;

  constructor(public sSchoolName, public nOfferPts, public nKRenewalPts, public nPRenewalPts, public nSRenewalPts, public bIsProspect=undefined, public sAddr, public sLocality, public nPobox, public sTown, public sState, public sCountry ){

    super(UUID.UUID(),sSchoolName,nOfferPts,nKRenewalPts,nPRenewalPts,nSRenewalPts,true);
    this.address = sAddr;
    this.pobox = nPobox;
    this.locality = sLocality;
    this.state = sState;
    this.country = sCountry;
    this.town = sTown;
  }

  get Address(){
    return this.address;
  }

  set Address(address){
    this.address = address;
  }

  get Pobox(){
    return this.pobox;
  }

  set Pobox(pobox){
    this.pobox = pobox;
  }

  get Locality(){
    return this.locality;
  }

  set Locality(locality){
    this.locality = locality;
  }

  get State(){
    return this.state;
  }

  set State(state){
    this.state = state;
  }

  get Country(){
    return this.country;
  }

  set Country(country){
    this.country = country;
  }
}
