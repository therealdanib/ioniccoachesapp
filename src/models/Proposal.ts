import { UUID } from 'angular2-uuid';

/**
 * Proposal class
 * @author Julio Bravo <jbravo@clb.unoi.com>
 */

export class Proposal {

    private proposalId:string;
    private creationDate;
    private schoolId:number = 0;
    private schoolName: string;
    private salesRepId:number;
    private features = [];
    private totalPts:number;
    private totalMxn:number;
    private totalYrs:number;
    private totalMxnExt:number;

    constructor(public school, public sName, public salesRep, public feats, public totalP, public totalCash, public totalY) {

      this.proposalId = UUID.UUID();
      this.schoolId = school;
      this.schoolName = sName;
      this.salesRepId = salesRep;
      this.creationDate = new Date();
      this.features = feats;
      this.totalPts = totalP;
      this.totalMxn = totalCash;
      this.totalYrs = totalY;
      this.totalMxnExt = this.totalMxn;
    }

    get ProposalId(){
      return this.proposalId;
    }

    set ProposalId(proposalId){
      this.proposalId = proposalId;
    }

    get CreationDate(){
      return this.creationDate;
    }

    get SchoolId(){
      return this.schoolId;
    }

    set SchoolId(schoolId){
      this.schoolId = schoolId;
    }

    get SchoolName(){
      return this.schoolName;
    }

    set SchoolName(schoolName){
      this.schoolName = schoolName;
    }

    get SalesRepId(){
      return this.salesRepId;
    }

    set SalesRepId(repId){
      this.salesRepId = repId;
    }

    get Features(){
      return this.features;
    }

    set Features(features){
      this.features = features;
    }

    get TotalPts(){
      return this.totalPts;
    }

    set TotalPts(totalPts){
      this.totalPts = totalPts;
    }

    get TotalMxn(){
      return this.totalMxn;
    }

    set TotalMxn(totalMxn){
      this.totalMxn = totalMxn;
    }

    get TotalYrs(){
      return this.totalYrs;
    }

    set TotalYrs(totalYrs){
      this.totalYrs = totalYrs;
    }

    get TotalMxnExt(){
      return this.totalMxnExt;
    }

    set TotalMxnExt(totalMxnExt){
      this.totalMxnExt = totalMxnExt;
    }
}
