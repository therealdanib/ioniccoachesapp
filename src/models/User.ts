/**
 * User class
 * @author Julio Bravo <jbravo@clb.unoi.com>
 */

export class User {

    private userId;
    private username;
    private firstName;
    private lastName;
    private email;
    private languageCode;

    constructor(public userid, public user, public name, public lastn, public mail, public lang) {

      this.userId = userid;
      this.username = user;
      this.firstName = name;
      this.lastName = lastn;
      this.email = mail;
      this.languageCode= lang;
    }

    public getUsername(){
        return this.username;
    }
    public setUsername(username){
        this.username = username;
    }
    public getFirstName(){
        return this.firstName;
    }
    public setFirstName(firstName){
        this.firstName = firstName;
    }
    public getLastName(){
        return this.lastName;
    }
    public setLastName(lastName){
        this.lastName = lastName;
    }
    public getEmail(){
        return this.email;
    }
    public setEmail(email){
        this.email = email;
    }
    public getLanguageCode(){
        return this.languageCode;
    }
    public setLanguageCode(lang){
        this.languageCode = lang;
    }
}
