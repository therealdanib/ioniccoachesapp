/**
 * School class
 * @author Julio Bravo <jbravo@clb.unoi.com>
 */

export class School{

  private schoolId:string;
  private isProspect:boolean = false;
  private schoolName:string;
  private offerPts:number;
  private kRenewalPts:number;
  private pRenewalPts:number;
  private sRenewalPts:number;

  constructor (public sSchoolId, public sSchoolName, public nOfferPts, public nKRenewalPts, public nPRenewalPts, public nSRenewalPts, public bIsProspect=undefined){

    this.schoolId = sSchoolId;
    this.schoolName = sSchoolName;
    this.offerPts = nOfferPts;
    this.kRenewalPts = nKRenewalPts;
    this.pRenewalPts = nPRenewalPts;
    this.sRenewalPts = nSRenewalPts;
    this.isProspect = bIsProspect !== undefined ? bIsProspect: this.isProspect;
  }

  get IsProspect(){
    return this.isProspect;
  }

  set IsProspect(isProspect){
    this.isProspect = isProspect;
  }

  public unsetProspect(){
    this.isProspect = false;
  }

  get SchoolId(){
    return this.schoolId;
  }

  set SchoolId(schoolId){
    this.schoolId = schoolId;
  }

  get SchoolName():string{
    return this.schoolName;
  }

  set SchoolName(schoolName){
    this.schoolName = schoolName;
  }

  get OfferPts(){
    return this.offerPts;
  }

  set OfferPts(offerPts){
    this.offerPts = offerPts;
  }

  get KRenewalPts(){
    return this.kRenewalPts;
  }

  set KRenewalPts(kRenewalPts){
    this.kRenewalPts = kRenewalPts;
  }

  get PRenewalPts(){
    return this.pRenewalPts;
  }

  set PRenewalPts(pRenewalPts){
    this.pRenewalPts = pRenewalPts;
  }

  get SRenewalPts(){
    return this.sRenewalPts;
  }

  set SRenewalPts(sRenewalPts){
    this.sRenewalPts = sRenewalPts;
  }
}
