import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SchoolService } from '../../providers/schools';
import { CreateRenewalPage } from '../create-renewal/create-renewal';

@Component({
  selector: 'page-renewal',
  templateUrl: 'renewal.html'
})
export class RenewalPage {

  public schools = [];
  public schoolsR = [];

  constructor(
    public navCtrl: NavController,
    public schoolSrv: SchoolService
  ) {}

  ionViewWillEnter() {
    this.schoolSrv.getSchools().then((schoolsData)=>{
      this.schools = schoolsData;
      this.loadSchools();
    });
  }

  loadSchools(){
    this.schoolsR = this.schools;
  }

  getItems(ev:any){

    this.loadSchools();
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.schoolsR = this.schools.filter((item) => {
        return (item.schoolName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  onCancel(ev:any){
    this.loadSchools();
  }

  createRenewal(school){
    this.navCtrl.push(CreateRenewalPage,{school:school});
  }

}
