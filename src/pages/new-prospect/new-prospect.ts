import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SchoolService } from '../../providers/schools';
import { CreateRenewalPage } from '../create-renewal/create-renewal';
import { Prospect } from '../../models/Prospect';
import { UiServices } from '../../providers/ui';
import 'rxjs/add/operator/finally';

@Component({
  selector: 'page-new-prospect',
  templateUrl: 'new-prospect.html'
})
export class NewProspectPage {

  public prospectName:string = '';
  public prospectAddr:string = '';
  public prospectPobox:number;
  public state:string = '';
  public town:string = '';
  public locality:string = '';
  public prospectForm : FormGroup;
  public availableLocal = [];
  public nKinder;
  public nElementary;
  public nSecondary;
  public nSum;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public schoolSrv: SchoolService,
    private uiSrv: UiServices
  ) {
    this.prospectForm = this.formBuilder.group({
      'prospectName': ['', [Validators.required, Validators.minLength(3)]],
      'prospectAddr': ['', [Validators.required, Validators.minLength(3)]],
      'prospectPobox': ['', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern('[0-9]*')]],
      'locality': ['',Validators.required],
      'nKinder': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'nElementary': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'nSecondary': ['', [Validators.required, Validators.pattern('[0-9]*')]]
    });
  }

  isValid(field: string) {
    let formField = this.prospectForm.controls[field];
    return formField.valid || formField.pristine;
  }

  zipChange(zipCode){

    if(zipCode !== undefined && zipCode.length === 5 && /^\d+$/.test(zipCode)){

      let loadingW = this.uiSrv.getLoading('Buscando colonias');
      this.schoolSrv.getLocalities(zipCode)
      .finally(() => {
        loadingW.dismiss();
      })
      .subscribe(
        data => {

          if(data.results !== undefined && data.results.colonias !== undefined){
            this.availableLocal = data.results.colonias;
            this.state = data.results.estado;
            this.town = data.results.municipio;
          }
          else{

            this.uiSrv.getToast('El código postal que ingresaste no existe. Intenta con algún otro',3000,'middle',true);
            this.clearLocation();
          }
        },
        err => {
          this.uiSrv.getToast('Hubo un error al traer la lista de colonias del código postal',3000,'middle',true);
          this.clearLocation();
        }
      );
    }
    else{
      this.clearLocation();
    }
  }

  getTotal(){

    var total = Math.floor((parseInt(this.nKinder)+parseInt(this.nSecondary)+parseInt(this.nElementary))/10);
    return isNaN(total) ? 0 : total;
  }

  clearLocation(){
    this.state = '';
    this.town = '';
    this.locality = '';
    this.availableLocal =[];
  }

  nextWindow(){

    var prospect = new Prospect(
      this.prospectName,
      this.getTotal(),
      this.nKinder,
      this.nElementary,
      this.nSecondary,
      true,
      this.prospectAddr,
      this.locality,
      this.prospectPobox,
      this.town,
      this.state,
      'Mexico'
    );
    this.navCtrl.push(CreateRenewalPage,{school:prospect});
  }
}
