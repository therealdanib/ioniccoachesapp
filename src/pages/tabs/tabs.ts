import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { NewProspectPage } from '../new-prospect/new-prospect';
import { RenewalPage } from '../renewal/renewal';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: any = HomePage;
  tab2Root: any = NewProspectPage;
  tab3Root: any = RenewalPage;

  constructor(public navCtrl: NavController) {}

}
