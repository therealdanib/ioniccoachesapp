import { Component } from '@angular/core';
import { App, NavController, ViewController, NavParams } from 'ionic-angular';
import { FeatureService } from '../../providers/features';
import { ProposalsSrv } from '../../providers/data';
import { UiServices } from '../../providers/ui';
import { Proposal } from '../../models/Proposal';
import { UserDataService } from '../../providers/user-data';
import { SchoolService } from '../../providers/schools';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/fromPromise';

@Component({
  selector: 'page-create-renewal',
  templateUrl: 'create-renewal.html'
})
export class CreateRenewalPage {

  public school;
  public features;
  public extends1yr = false;
  public extraPts = 0;
  public initialPts = 0;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public featureSrv: FeatureService,
    public proposalsSrv: ProposalsSrv,
    public uiSrv: UiServices,
    public usrService: UserDataService,
    public schoolSrv: SchoolService,
    public app: App
  ){
    this.loadSchoolData();
  };

  loadSchoolData(){
    this.school = this.navParams.get('school');
    this.initialPts = this.school.offerPts;
    this.extraPts = Math.floor(this.school.offerPts / 3);
    this.features = this.featureSrv.getFeatures();
    this.features.sort((feature1,feature2) => feature1['order'] - feature2['order']);
  }

  incrementValue(feature){
    feature.value++;
    this.school.offerPts -= feature.points;
  }

  decrementValue(feature){
    feature.value--;
    this.school.offerPts += feature.points;
  }

  getTotal(){
    return this.school.offerPts < 0 ? Math.abs(this.school.offerPts)*17500 : 0;
  }

  useExtra(){
    this.extends1yr = !this.extends1yr;
    this.school.offerPts = this.extends1yr ? this.school.offerPts+this.extraPts : this.school.offerPts-this.extraPts;
  }

  onBalancelongPress(event:any){
    this.useExtra();
  }

  close(){
    let timeoutOff = setTimeout(() => {
      this.app.getRootNav().getActiveChildNav().select(0);
      this.viewCtrl.dismiss();
      clearTimeout(timeoutOff);
    }, 1);
  }

  saveProposal(){

    Observable.forkJoin([
      Observable.fromPromise(this.usrService.getUserData()),
      Observable.fromPromise(this.proposalsSrv.getProposals()),
      Observable.fromPromise(this.schoolSrv.getSchools()),
    ]).subscribe(
      data => {

        var user = data[0];
        var proposals = data[1] === null ? [] : data[1];
        var schools = data[2] === null ? [] : data[2];

        var newProposal = new Proposal(
          this.school.schoolId,
          this.school.schoolName,
          user.userId,
          this.features,
          this.school.offerPts < 0 ? (this.initialPts+Math.abs(this.school.offerPts)) : this.initialPts-this.school.offerPts,
          this.getTotal(),
          this.extends1yr ? 2 : 1
        );

        if(this.school.isProspect){

          var found = schools.filter((school) =>{
            return (school.schoolId.toLowerCase().indexOf(this.school.schoolId.toLowerCase())>-1);
          });
          if(found.length === 0){
            this.school.offerPts = this.initialPts;
            schools.push(this.school);
            this.schoolSrv.updateSchools(schools);
          }
        }

        proposals.push(newProposal);
        this.proposalsSrv.updateProposals(proposals);
        this.uiSrv.getToast('Se guardó la propuesta con éxito',2000,'middle');
        this.close();
      },
      err => {
        this.uiSrv.getToast('Hubo un error al guardar la propuesta',2000,'middle',true);
      }
    );
  }
}
