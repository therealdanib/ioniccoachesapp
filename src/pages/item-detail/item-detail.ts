import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {

  public proposal;

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController
  ){
    this.proposal = this.navParams.get('proposal');
  }
}
