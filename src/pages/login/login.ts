import { Component } from '@angular/core';
import { NavController,Platform,ToastController} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import 'rxjs/add/operator/finally';

import { LoginService } from '../../providers/login-service';
import { UserDataService } from '../../providers/user-data';
import { UiServices } from '../../providers/ui';
import { SchoolService } from '../../providers/schools';

import { School } from '../../models/School';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  isTouchAvailable: boolean;
  user = '';
  passwd = '';
  loginForm : FormGroup;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    private _platform: Platform,
    private loginSrv: LoginService,
    private usrService: UserDataService,
    public toastCtrl: ToastController,
    private uiSrv: UiServices,
    public userSrv: UserDataService,
    public schoolSrv: SchoolService
  ) {

    this.loginForm = this.formBuilder.group({
      'username': ['', [Validators.required, Validators.minLength(3)]],
      'password': ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  ionViewWillEnter() {

    this.userSrv.getUserData().then((user)=>{

      if(user){
        this.navCtrl.setRoot(TabsPage);
      }
    });
  }

  isValid(field: string) {
    let formField = this.loginForm.controls[field];
    return formField.valid || formField.pristine;
  }

  validateLogin(user,passwd){

    var schools = [];
    let loadingW = this.uiSrv.getLoading('Ingresando ...');
    this.loginSrv.validateLogin(user,passwd)
    .finally(() => {
      loadingW.dismiss();
    })
    .subscribe(
      data => {
        var resultData = data;
        if(resultData.error){
          this.uiSrv.getToast('Nombre de usuario o contraseña incorrectos',2000,'middle',true);
        }
        else{

          let loadDataW = this.uiSrv.getLoading('Obteniendo datos ...');
          this.schoolSrv.getSchoolCatalog().finally(()=>{
            loadDataW.dismiss();
            loadingW.dismiss();
          })
          .subscribe(
            schoolsData =>{

              if(schoolsData.results !== undefined){

                schoolsData.results.forEach((school)=>{
                  schools.push(new School(
                    school.colegioId.toString(),
                    school.colegio,
                    school.data.puntosOfrecer,
                    school.data.renovacionK,
                    school.data.renovacionP,
                    school.data.renovacionS,
                    false
                  ));
                })
                this.schoolSrv.updateSchools(schools);
                this.usrService.saveUserData(resultData.person);
                this.navCtrl.setRoot(TabsPage);
              }else{
                this.uiSrv.getToast('Hubo un error cargando la lista de colegios',2000,'middle',true);
              }
            },
            err => {
              this.uiSrv.getToast('Hubo un error cargando los datos desde el servidor de UNOi',2000,'middle',true);
            }
          );
        }
      },
      err => {
        this.uiSrv.getToast('Lo sentimos. Hubo un error tratando de iniciar sesión',2000,'middle',true);
      }
    );
  }
}
