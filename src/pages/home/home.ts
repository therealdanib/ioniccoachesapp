import { Component } from '@angular/core';
import { AlertController, ItemSliding, NavController } from 'ionic-angular';
import { ProposalsSrv } from '../../providers/data';
import { ItemDetailPage } from '../item-detail/item-detail';
import { UserDataService } from '../../providers/user-data';
import { UiServices } from '../../providers/ui';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  public proposals = [];
  public proposalsR = [];
  public firstName:string;
  public mailRegex = /^(([^<>()[\]{}'^?\\.,!|//#%*-+=&;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

  constructor(
    public propSrv: ProposalsSrv,
    public navCtrl: NavController,
    public userSrv: UserDataService,
    public alertCtrl: AlertController,
    public uiSrv: UiServices
  ){}

  ionViewWillEnter(){

    this.propSrv.getProposals().then((props)=>{
      this.proposals = props !== undefined && props !== null ? props : this.proposals;
      this.loadProposals();
    });

    this.userSrv.getUserData().then((user)=>{
      this.firstName = user.firstName;
    })
  }

  loadProposals(){
    this.proposalsR = this.proposals;
  }

  viewProposal(proposal){
    this.navCtrl.push(ItemDetailPage,{proposal:proposal});
  }

  getItems(ev:any){

    this.loadProposals();
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.proposalsR = this.proposals.filter((item) => {
        return (item.schoolName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  presentAlert(){

    let alert = this.alertCtrl.create({
      title: 'Enviar propuesta',
      subTitle: 'Ingresa el nombre y correo electrónico de la persona que recibirá esta propuesta',
      inputs: [
        {
          name: 'contactName',
          placeholder: 'Nombre'
        },
        {
          name: 'contactMail',
          placeholder: 'Correo electrónico',
          type: 'email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Enviar',
          handler: data => {
            if (data.contactName && data.contactMail) {

              if(this.mailRegex.test(data.contactMail)){
                //Here, you can add a method for sending email
                this.uiSrv.getToast('La propuesta se ha enviado con éxito',3000,'middle');
              }
              else{
                this.uiSrv.getToast('No ingresaste un correo electrónico válido',3000,'middle',true);
              }
            } else {
              this.uiSrv.getToast('Debes ingresar los datos necesarios para enviar la propuesta',3000,'middle',true);
            }
          }
        }
      ]
    });
    alert.present();
  }

  onCancel(ev:any){
    this.loadProposals();
  }

  closeSliding(slidingItem: ItemSliding){
    slidingItem.close();
  }
}
