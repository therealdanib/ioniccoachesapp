//Core Modules
import { NgModule, enableProdMode } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Storage } from '@ionic/storage';

//Application Pages
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ItemDetailPage } from '../pages/item-detail/item-detail';
import { NewProspectPage } from '../pages/new-prospect/new-prospect';
import { RenewalPage } from '../pages/renewal/renewal';
import { CreateRenewalPage } from '../pages/create-renewal/create-renewal';
import { LoginPage } from '../pages/login/login';

//Application Services
import { ProposalsSrv } from '../providers/data';
import { SchoolService } from '../providers/schools';
import { LoginService } from '../providers/login-service';
import { UserDataService } from '../providers/user-data';
import { FeatureService } from '../providers/features';
import { UiServices } from '../providers/ui';

enableProdMode();

/**
 * Global component for application
 */

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    NewProspectPage,
    RenewalPage,
    CreateRenewalPage,
    LoginPage,
    HomePage,
    ItemDetailPage
  ],
  imports: [
    IonicModule.forRoot(MyApp,{
      tabsHideOnSubPages: true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    NewProspectPage,
    RenewalPage,
    CreateRenewalPage,
    LoginPage,
    HomePage,
    ItemDetailPage
  ],
  providers: [Storage, ProposalsSrv, SchoolService, FeatureService, LoginService, UserDataService, UiServices]
})

export class AppModule {}
