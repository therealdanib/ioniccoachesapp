import { Injectable } from '@angular/core';
import { LoadingController,ToastController } from 'ionic-angular';

@Injectable()
export class UiServices {

  constructor(
    private toastCtrl: ToastController,
    private loadCtrl: LoadingController
  ){}

  getToast(msg,duration,pos,hasError=false){
    this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: pos,
      cssClass: hasError ? "error-toast" : "toastyle"
    }).present();
  };

  getLoading(title,length=undefined){

    var loadObj = this.loadCtrl.create({
      "spinner":"crescent",
      "content": title,
      "duration": length
    });
    loadObj.present();
    return loadObj;
  }
}
