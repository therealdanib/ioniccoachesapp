import { Injectable } from '@angular/core';

@Injectable()
export class FeatureService {

  constructor() {}

  getFeatures(){
    return [
      {
        "order":1,
        "name": "Aula Digital",
        "points": 2,
        "value":0
      },
      {
        "order":2,
        "name": "Maker Cart",
        "points": 3,
        "value":0
      },
      {
        "order":3,
        "name": "Aula Maker",
        "points": 18,
        "value":0
      },
      {
        "order":4,
        "name": "Proyecto Interactivo",
        "points": 2,
        "value":0
      },
      {
        "order":5,
        "name": "Telepresencia",
        "points": 20,
        "value":0
      },
      {
        "order":6,
        "name": "Aceleración de contenido",
        "points": 4,
        "value":0
      },
      {
        "order":7,
        "name": "Certificación ISO",
        "points": 12,
        "value":0
      },
      {
        "order":8,
        "name": "Desarrollo profesional Apple",
        "points": 20,
        "value":0
      },
      {
        "order":9,
        "name": "Certificación ETS",
        "points": 1,
        "value":0
      }
    ];
  }

}
