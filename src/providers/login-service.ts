import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  loginEndpoint = 'https://www.sistemauno.com/source/ws/uno_wsj_login.php?user=';

  constructor(
    public http: Http
  ){}

  validateLogin(user,password) {
    var url = this.loginEndpoint+user+"&password="+password;
    return this.http.get(url).map(res => res.json());
  }

}
