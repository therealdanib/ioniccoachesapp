import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SchoolService } from './schools';

@Injectable()
export class ProposalsSrv {

  constructor(
    public storage: Storage,
    public schoolSrv: SchoolService,
  ) {}

  getProposals(){
    return this.storage.get('proposals');
  }

  updateProposals(proposals){
    let newData = proposals;
    this.storage.set('proposals',newData);
  }

}
