import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class SchoolService {

  schoolsEndpoint = 'https://ruta.unoi.com/api/v0/simCosts/colegioPuntos';
  localitiesEndpoint = 'https://ruta.unoi.com/api/v0/simCosts/cp/';

  constructor(
    public http: Http,
    public storage: Storage
  ){}

  getSchoolCatalog(){
    var url = this.schoolsEndpoint;
    return this.http.get(url).map(res => res.json());
  }

  getSchools(){
    return this.storage.get('schools');
  }

  updateSchools(schools){
    let newData = schools;
    this.storage.set('schools',newData);
  }

  getLocalities(zipCode){
    var url = this.localitiesEndpoint+zipCode;
    return this.http.get(url).map(res => res.json());
  }

}
