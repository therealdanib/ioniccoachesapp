import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from '../models/User';

@Injectable()
export class UserDataService {

  constructor(public storage: Storage) {}

  saveUserData(userData){

    var user = new User(userData.personId,userData.user,userData.name,userData.surname,userData.email,userData.languageCode);
    this.storage.set('userData',user);
  }

  getUserData(){
    return this.storage.get('userData');
  }
}
